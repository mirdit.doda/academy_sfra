'use strict';

var guard = require('academy_controllers/cartridge/scripts/guard');
var ISML = require('dw/template/ISML');


function start() {
	var myParam = request.httpParameterMap;
	var parameter = myParam.param.stringValue;

	if (parameter != null) {
		ISML.renderTemplate(
			'call/jnotEmpty.isml', { paramOnPdict: parameter }
		);

	}
	else {
		ISML.renderTemplate(
			'call/jempty.isml', { paramOnPdict: 'param not found' }
		);
	};
};

exports.Start = guard.ensure(['get'], start);