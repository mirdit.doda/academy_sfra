'use strict';

var guard = require('academy_controllers/cartridge/scripts/guard');

function debug() {return true;}

exports.debug = guard.ensure(['get'], debug);
