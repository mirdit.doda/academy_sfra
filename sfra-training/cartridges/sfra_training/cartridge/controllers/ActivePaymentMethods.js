'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var stringUtils = require('dw/util/StringUtils');
//var Site = require('dw/system/Site');
//var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
var urlUtils = require('dw/web/URLUtils');
//var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');
//var calendar = require("dw/util/Calendar");
var paymentMgr = require('dw/order/PaymentMgr');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {

        var payments = paymentMgr.getActivePaymentMethods()

        for (i = 0 ; i < payments.length; i++) {
            var listFE = paymentMgr.getActivePaymentMethods().get(i);
            res.json(listFE);
        }
        
        next();
    }
);

module.exports = server.exports();