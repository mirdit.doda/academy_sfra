'use strict';

var server = require('server');
var cache = require('*/cartridge/scripts/middleware/cache');
var stringUtils = require('dw/util/StringUtils');
//var Site = require('dw/system/Site');
//var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
var urlUtils = require('dw/web/URLUtils');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction')
var calendar = require("dw/util/Calendar");

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {

       
        
        var objectId = {};
        objectId.Id = dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterobjectName2");
        objectId.name = req.querystring.name;
        objectId.last_name = req.querystring.last_name;
        objectId.email = req.querystring.email;

        transaction.wrap(function () {

            if (objectId.email != null && objectId.Id != null) {

                var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
                var customObject = customObjectMgr.createCustomObject(objectId.Id, currentTimeStamp);

                customObject.custom.name = objectId.name;
                customObject.custom.last_name = objectId.last_name;
                customObject.custom.email = objectId.email;

                var jsonResponse = {};
                jsonResponse.success = true;
                jsonResponse.currentTimeStamp = customObject.custom.timestamp;
            } else {
                var jsonResponse = {};
                jsonResponse.success = false;
            };




        
            res.json(jsonResponse);

        });

        next();

    }

);
//


server.get(
    'Read',
    server.middleware.https,
    function (req, res, next) {
        transaction.wrap(function () {

            var customList = customObjectMgr.getAllCustomObjects("newsletter2");
            var listFE = [];

            while (customList.hasNext()) {
                var costomObjectList = customList.next();
                var item = {}
                item.timestamp = costomObjectList.custom.timestamp;
                item.name = costomObjectList.custom.name;
                item.last_name = costomObjectList.custom.last_name;
                item.email = costomObjectList.custom.email;
                listFE.push(item);
            }

            res.json(listFE);

        });

        next();
    }
);

server.get(
    'Remove',
    server.middleware.https,
    function (req, res, next) {

        var objectId = {};
        objectId.type = "newsletter2"
        objectId.key = req.querystring.key;

        transaction.wrap(function () {

            if (objectId.key != null) {

                var objectToRemove = customObjectMgr.getCustomObject(objectId.type, objectId.key);
                customObjectMgr.remove(objectToRemove);

                var jsonResponse = {};
                jsonResponse.success = true;
            } else {
                var jsonResponse = {};
                jsonResponse.success = false;
            }
            res.json(jsonResponse);

        });

        next();
    }
);

module.exports = server.exports();