'use strict';

var server = require('server');
//var cache = require('*/cartridge/scripts/middleware/cache');
//var stringUtils = require('dw/util/StringUtils');
var site = require('dw/system/Site');
//var pageMetaHelper = require('*/cartridge/scripts/helpers/pageMetaHelper');
//var urlUtils = require('dw/web/URLUtils');
//var transaction = require('dw/system/Transaction');
var productMgr = require('dw/catalog/ProductMgr');
var hookMgr = require('dw/system/HookMgr');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {

        var isAcademyProductActive = site.getCurrent().getCustomPreferenceValue("isAcademyProductActive");
        var productId = site.getCurrent().getCustomPreferenceValue("academyProductId");

        if (isAcademyProductActive == true && productId != null) {

            var product = {};
            product.properties = productMgr.getProduct(productId);
            product.creator = "academyProduct";

            var customObject = hookMgr.callHook('createObjectProduct', 'createObjectProduct', product);

            res.render('academyProduct/academyProduct.isml', {
                productisml: customObject.custom,
                //status: "succes"
            })
        } else {
            var errorMsg = hookMgr.callHook('test', 'test', null)
            res.render('academyProduct/academyProductError.isml', {
                error: errorMsg
            })
        }

        next();
    }
);

module.exports = server.exports();