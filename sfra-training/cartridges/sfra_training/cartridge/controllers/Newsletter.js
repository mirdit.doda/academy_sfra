'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
var newsletterForm = server.forms.getForm('newsletter');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction')
var Site = require('dw/system/Site');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next) {

        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: URLUtils.url('Newsletter-Subscribe'),
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.post(
    'Subscribe',
    server.middleware.https,
    function (req, res, next) {

        var service = newsletterHelpers.getService('crm.newsletter.subscribe');
        service.URL = service.URL + '?hapikey=' + Site.getCurrent().getCustomPreferenceValue("newsletterKey");

        var email = newsletterForm.email.htmlValue;
        var fname = newsletterForm.fname.htmlValue;
        var lname = newsletterForm.lname.htmlValue;

        var reqObject = {
            properties: {
                //company: "Biglytics_prova",
                email: email,
                firstname: fname,
                lastname: lname
                //phone: "(877) 929-0687",
                //website: "biglytics.net"
            }
        };

        var serverErrorSimulation = 0;
        if (serverErrorSimulation == 0) {
            var response = service.call(reqObject);
        } else if (serverErrorSimulation == 1) {
            //SIMULATING SERVER ERROR
            var response = {
                ok: false,
                msg: "server unavailable"
            };
        };

        var jsonResponse = {};

        if (response.ok) {
            var contactID = JSON.parse(response.object).id;
            jsonResponse = {
                email: email,
                name: fname,
                last_name: lname,
                id: contactID,
                response: response.msg + '. Id: ' + contactID,
                success: true
                //redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()
            };
        } else if (response.error == 409) {
            jsonResponse = {
                response: JSON.parse(response.errorMessage).message,
                success: false,
            };

        } else {

            transaction.wrap(function () {

                var customObjecttype = 'newsletter-call';
                var customObject = customObjectMgr.getCustomObject(customObjecttype, email);
                //var allSavedUsers = dw.object.CustomObjectMgr.getAllCustomObjects(customObjecttype);

                if (customObject == null) { // in allSavedUsers.email) {
                    customObject = customObjectMgr.createCustomObject(customObjecttype, email);
                    customObject.custom.name = fname;
                    customObject.custom.lname = lname;
                    var errorMsg = 'Your data has been saved and we will try to send it again soon';
                } else {
                    //var customObject = customObjectMgr.getCustomObject(customObjecttype, email)
                    var errorMsg = 'Your email is already saved but it has not been sent to the server yet';
                };

                jsonResponse = {
                    response: 'There was an error: ' + response.msg + '. ' + errorMsg,
                    success: false
                };
            });
        };

        res.json(jsonResponse);
        next();
    }
);

server.get(
    'CustomerList',
    server.middleware.https,
    function (req, res, next) {

        var service = newsletterHelpers.getService('crm.newsletter.subscribe');
        service.URL = service.URL + '?hapikey=' + Site.getCurrent().getCustomPreferenceValue("newsletterKey");
        service.requestMethod = 'GET';


        var numberOfCustomers = req.querystring.numberOfCustomers;
        if (numberOfCustomers == null) {
            numberOfCustomers = 30;
        };
        service.URL = service.URL + '&limit=' + numberOfCustomers;

        var response = service.call();

        if (response.ok) {
            var customerList = JSON.parse(response.object).results;
            res.render('newsletter/newsletterCustomerList', {
                customerList: customerList
            });
        } else {
            res.json({
                response: 'There was an error: ' + response.msg,
                success: false
            });
        };

        next();
    }
);

server.get(
    'CustomerDelete',
    server.middleware.https,
    function (req, res, next) {

        var service = newsletterHelpers.getService('crm.newsletter.subscribe');
        var deleteId = req.querystring.deleteId;
        service.URL = service.URL + '/' + deleteId + '?hapikey=' + Site.getCurrent().getCustomPreferenceValue("newsletterKey");
        service.requestMethod = 'DELETE';
        var response = service.call();

        if (response.ok) {
            res.json({
                deleteId: deleteId,
                success: true
            });
        } else {
            res.json({
                response: 'There was an error: ' + response.msg,
                success: false
            });
        };

        next();
    }
);

server.get(
    'Edit',
    server.middleware.https,
    function (req, res, next) {

        newsletterForm.clear();
        var properties = {};
        properties.newsletterForm = newsletterForm;
        properties.actionURL = URLUtils.url('Newsletter-CustomerUpdate');
        properties.updateId = req.querystring.updateId;
        properties.updateName = req.querystring.updateName;
        properties.updateLastName = req.querystring.updateLastName;
        properties.updateEmail = req.querystring.updateEmail;

        res.render('/newsletter/newsletterUpdate', {
            properties: properties
        });
        next();
    }
);

server.post(
    'CustomerUpdate',
    server.middleware.https,
    function (req, res, next) {

        var service = newsletterHelpers.getService('crm.newsletter.subscribe');

        var reqObject = {
            properties: {
                //company: "Biglytics_prova",
                email: newsletterForm.email.htmlValue,
                firstname: newsletterForm.fname.htmlValue,
                lastname: newsletterForm.lname.htmlValue
                //phone: "(877) 929-0687",
                //website: "biglytics.net"
            }
        };

        service.URL = service.URL + '/' + req.form.updateId + '?hapikey=' + Site.getCurrent().getCustomPreferenceValue("newsletterKey");
        service.requestMethod = 'PATCH';
        var response = service.call(reqObject);

        var jsonResponse = {};

        if (response.ok) {
            jsonResponse = {
                response: "Successfully updated",
                success: true
                //redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()
            };
        } else {
            jsonResponse = {
                response: 'There was an error: ' + response.msg,
                success: false
            };
        };

        res.json(jsonResponse);
        next();
    }
);

server.post(
    'Handler',
    server.middleware.https,
    function (req, res, next) {

        var continueUrl = URLUtils.url('Newsletter-Show');

        // Perform any server-side validation before this point, and invalidate form accordingly
        if (newsletterForm.valid) {
            // Show the success page
            res.json({
                success: true,
                redirectUrl: URLUtils.url('Newsletter-Success').toString()
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Error-Start').toString()
            });
        }

        next();
    }
);

server.get(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/newslettersuccess', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')
        });

        next();
    }
);



module.exports = server.exports();