'use strict';

module.exports = function () {
    $('form.deleteCustomer').on('click', function (e) {
        var $form = $(this);
        e.preventDefault();
        var url = $form.attr('action');
        $form.spinner().start();
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                console.log(data);
                $form.spinner().stop();
                if (data.success == true) {
                    alert("Customer with Id " + data.deleteId + " was successfully removed");
                    location.reload();
                } else {
                    alert("Could not delete customer");
                }
            },
            error: function (err) {
                // if (err.responseJSON.redirectUrl) {
                //     window.location.href = err.responseJSON.redirectUrl;
                // }
                $form.spinner().stop();
            }
        });
        return false;
    });
};