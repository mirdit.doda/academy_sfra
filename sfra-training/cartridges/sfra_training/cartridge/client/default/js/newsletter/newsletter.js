'use strict';

var formValidation = require('base/components/formValidation');

module.exports = function () {
    $('form.newsletter-form').on('submit', function (e) {
        var $form = $(this);
        e.preventDefault();
        var url = $form.attr('data-url');
        $form.spinner().start();
        $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: $form.serialize(),
            success: function (data) {
                console.log(data);
                $form.spinner().stop();
                if (!data.success) {
                    $('.subscription-feedback').text(data.response);
                    $('.subscription-feedback').addClass('failure');
                    $('.newsletter-result').removeClass('hidden');
                    $('#gobacklink').css("visibility", "visible");
                } else {
                    $('.subscription-feedback').text(data.response);
                    $('.subscription-feedback').addClass('success');
                    $('.newsletter-result').removeClass('hidden');
                    $('#gobacklink').css("visibility", "visible");
                }
            },
            error: function (err) {
                if (err.responseJSON.redirectUrl) {
                    window.location.href = err.responseJSON.redirectUrl;
                }
                $form.spinner().stop();
            }
        });

        return false;
    });
};