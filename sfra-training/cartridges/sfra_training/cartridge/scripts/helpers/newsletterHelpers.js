'use strict';

var LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
/**
 * Fetcehs the local service registry assignied to a service Id
 * @param {string} serviceId - service ID
 * @returns {Object} local service registry
 */
function getService(serviceId) {

    // var callTestGet = LocalServiceRegistry.createService("test.http.get", {
    //     createRequest: function(svc: HTTPService, args) {
    //         svc.setRequestMethod("GET");
    //     },
    //     parseResponse: function(svc: HTTPService, client: HTTPClient) {
    //         return client.text;
    //     },
    //     mockCall: function(svc: HTTPService, client: HTTPClient) {
    //         return {
    //             statusCode: 200,
    //             statusMessage: "Success",
    //             text: "MOCK RESPONSE (" + svc.URL + ")"
    //         };
    //     },
    //     filterLogMessage: function(msg: String) {
    //         return msg.replace("headers", "OFFWITHTHEHEADERS");
    //     }
    // });


    return LocalServiceRegistry.createService(serviceId, {
        createRequest: function (svc, args) {

            svc.addHeader("Content-Type", "application/json");
            svc.addHeader("Accept", "application/json");
            // Default request method is post
            // No need to setRequestMethod
            if (args) {

                return JSON.stringify(args);
            } else {
                return null;
            }
        },
        parseResponse: function (svc, client) {
            return client.text;
        },
        mockCall: function (svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }
    });
}

module.exports = {
    getService: getService
};