'use strict';

function debug() {
    return true;
}

module.exports.debug = debug;

/*

<isscript>
var debug = require('sfra_training/cartridge/scripts/DebuggerTemplate.js').debug();
var fine = 'fine';
</isscript>

*/