'use strict'

var customObjectLog = function () {

    var args = arguments[0];
    var allObjects = dw.object.CustomObjectMgr.getAllCustomObjects(args.customObjectType);
    var customLogger = dw.system.Logger.getLogger('CustomObject', 'warn');

    while (allObjects.hasNext()) {
        var ObjectList = allObjects.next();
        customLogger.warn('ID: {0}, name: {1}, description: {2}',
            ObjectList.custom.ID,
            ObjectList.custom.name,
            ObjectList.custom.description
        );
    }
}

exports.customObjectLog = customObjectLog;