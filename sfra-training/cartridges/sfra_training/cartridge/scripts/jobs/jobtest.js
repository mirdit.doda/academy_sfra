'use strict';

var Status = require('dw/system/Status');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction')
var Logger = require('dw/system/Logger');
var customLogger = Logger.getLogger('IdNotFound','warn');

var jobTest = function () {

    var args = arguments[0];
    var customObject = {};
    customObject.type = args.TestParam;
    customObject.key = args.testParam2;
    var objectToRemove = customObjectMgr.getCustomObject(customObject.type, customObject.key);
    
    if (objectToRemove != null) {
        transaction.wrap(function () {
            customObjectMgr.remove(objectToRemove);
        })
        return (new Status(Status.OK));
    } else {
        return (new Status(Status.ERROR));
        customLogger.error('Object to remove not found');
    }
}

exports.JobTest = jobTest;