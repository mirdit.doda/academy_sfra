'use strict'

var sendNewsletterData = function (args) {

    var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
    var service = newsletterHelpers.getService('crm.newsletter.subscribe');
    var customObjectMgr = require('dw/object/CustomObjectMgr');
    var transaction = require('dw/system/Transaction');
    var customLogger = dw.system.Logger.getLogger('SendNewsletterData', 'warn');
    var Status = require('dw/system/Status');

    service.URL = service.URL + '?hapikey=' + dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterKey");
    //var allObjects = customObjectMgr.queryCustomObjects(storeHelpers.config.args.customObjectType, 'custom.isEmailSent != {0}', 'creationDate ASC', true);
    var allObjects = customObjectMgr.getAllCustomObjects(args.customObjectType);
    var outputList = [];
    
    while (allObjects.hasNext()) {
        var customObject = allObjects.next();
        var reqObject = {
            properties: {
                email: customObject.custom.email,
                firstname: customObject.custom.name,
                lastname: customObject.custom.lname
            }
        };

        var serverErrorSimulation = 1;
        if (serverErrorSimulation == 0) {
            var response = service.call(reqObject);
        } else if (serverErrorSimulation == 1) {
            //SIMULATING SERVER ERROR
            var response = {
                ok: false,
                msg: "server unavailable"
            };
        };

        outputList.push([customObject.custom.email + ": " + response.msg]);

        if (response.ok || response.error == 409) {
            transaction.wrap(function () {
                customObjectMgr.remove(customObject);
            })
        };
    };

    customLogger.warn(outputList);

    if (customObjectMgr.getAllCustomObjects(args.customObjectType).count == 0) {
        return new Status(Status.OK)
    } else {
        return new Status(Status.ERROR)
    };
    
};

exports.sendNewsletterData = sendNewsletterData;