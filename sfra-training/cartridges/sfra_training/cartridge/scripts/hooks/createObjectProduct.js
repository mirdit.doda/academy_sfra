'use strict';

function createObjectProduct(product) {

    var calendar = require("dw/util/Calendar");
    var stringUtils = require('dw/util/StringUtils');

    var currentTimeStamp = stringUtils.formatCalendar(new calendar(new Date()), "yyyyMMddHHmmssSS");
    var customObject = {};
    dw.system.Transaction.wrap(function () {

        product.ID = product.properties.ID + "_" + currentTimeStamp;
        customObject = dw.object.CustomObjectMgr.createCustomObject(product.creator, product.ID);
        customObject.custom.name = product.properties.name;
        customObject.custom.description = product.properties.shortDescription;
    });
    return customObject;

}

exports.createObjectProduct = createObjectProduct;