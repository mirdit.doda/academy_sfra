'use strict';

var Template = require('dw/util/Template');
var HashMap = require('dw/util/HashMap');

/**
 * Render logic for the storefront.photoTile component.
 * @param {dw.experience.ComponentScriptContext} context The Component script context object.
 * @param {dw.util.Map} [modelIn] Additional model values created by another cartridge. This will not be passed in by Commcerce Cloud Plattform.
 *
 * @returns {string} The markup to be displayed
 */
module.exports.render = function (context, modelIn) {
    var model = modelIn || new HashMap();
    var content = context.content;

    model.videourl = context.content.videourl;
    model.platformselector = context.content.platformselector;

    switch (context.content.platformselector) {
        case "YouTube":
            model.platform.width="560";
            model.platform.heigth="315";
            model.platform.frameborder="0";
            model.platform.allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture";
        break;

        case "Vimeo":
        model.platform.width="640";
        model.platform.heigth="360";
        model.platform.frameborder="0";
        model.platform.allow="autoplay; fullscreen; picture-in-picture";
        break;

        default:
        break;
    }

    return new Template('experience/components/commerce_assets/ytvideo').render(model).text;
};
